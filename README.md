**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).


[autodeda.ru](https://autodeda.ru)

[autodziadek.pl](https://autodziadek.pl)

[autograndad.com](https://autograndad.com)

[bluerock.es](https://bluerock.es)

[buy-com.de](https://buy-com.de)

[buy-com.in](https://buy-com.in)

[buy-com.pl](https://buy-com.pl)

[buy-com.ru](https://buy-com.ru)

[buy-es.com](https://buy-es.com)

[buy-us.net](https://buy-us.net)

[e-hokkaido.in](https://e-hokkaido.in)

[freejournal.info](https://freejournal.info)

[freejournal.org](https://freejournal.org)

[gamesnew.info](https://gamesnew.info)

[googl-info.com](https://googl-info.com)

[google-cn.info](https://google-cn.info)

[google-es.info](https://google-es.info)

[google-fr.info](https://google-fr.info)

[google-info.cn](https://google-info.cn)

[google-info.de](https://google-info.de)

[google-info.in](https://google-info.in)

[google-info.org](https://google-info.org)

[google-info.pl](https://google-info.pl)

[google-wiki.info](https://google-wiki.info)

[info-about.in](https://info-about.in)

[info-about.info](https://info-about.info)

[info-about.net](https://info-about.net)

[info-about.org](https://info-about.org)

[info-about.ru](https://info-about.ru)

[info-com.in](https://info-com.in)

[infon.in](https://infon.in)

[media-inform.com](https://media-inform.com)

[my-greenday.de](https://my-greenday.de)

[sexinfon.com](https://sexinfon.com)

[shops-com.es](https://shops-com.es)

[shops-com.in](https://shops-com.in)

[shops-com.ru](https://shops-com.ru)

[shops-net.com](https://shops-net.com)

[store-com.de](https://store-com.de)

[vaskar.co.in](https://vaskar.co.in)

[what-this.com](https://what-this.com)

[wikipedy.org](https://wikipedy.org)

[shops-com.pl](https://shops-com.pl)

[westavto.pp.ua](https://westavto.pp.ua)

[dokument.pp.ua](https://dokument.pp.ua)

[ago.pp.ua](https://ago.pp.ua)

[zlatovlaska.pp.ua](https://zlatovlaska.pp.ua)

[vityska.pp.ua](https://vityska.pp.ua)

[depression.pp.ua](https://depression.pp.ua)

[pino.pp.ua](https://pino.pp.ua)

[xn----7sbabogzn4bbfdi4a.pp.ua](https://xn----7sbabogzn4bbfdi4a.pp.ua)

[xn----8sbiec5bocbhi.pp.ua](https://xn----8sbiec5bocbhi.pp.ua)

[xn----7sbiewaowdbfdjyt.pp.ua](https://xn----7sbiewaowdbfdjyt.pp.ua)

[xn----8sbnsidjdcctjpc4m.pp.ua](https://xn----8sbnsidjdcctjpc4m.pp.ua)

[xn----7sbohijc0apczgk3k.pp.ua](https://xn----7sbohijc0apczgk3k.pp.ua)

[xn----8sbptrdxc5b.pp.ua](https://xn----8sbptrdxc5b.pp.ua)

[xn----0tbbnc0a9b.pp.ua](https://xn----0tbbnc0a9b.pp.ua)

[xn--90a7ad6b.pp.ua](https://xn--90a7ad6b.pp.ua)

[xn-----8kcgrneke6aorvlg4l.pp.ua](https://xn-----8kcgrneke6aorvlg4l.pp.ua)

[xn----8sbeookxvz.pp.ua](https://xn----8sbeookxvz.pp.ua)

[sharovaya-opora.pp.ua](https://sharovaya-opora.pp.ua)

[xn----7sbabh5edaieh0gue.pp.ua](https://xn----7sbabh5edaieh0gue.pp.ua)

[maslyanyy-filtr-lanos.pp.ua](https://maslyanyy-filtr-lanos.pp.ua)

[xn-----6kcc2bendbcnhnximk7a4kza9c.pp.ua](https://xn-----6kcc2bendbcnhnximk7a4kza9c.pp.ua)

[xn--80apjujj2d.pp.ua](https://xn--80apjujj2d.pp.ua)

[avtodid.pp.ua](https://avtodid.pp.ua)

[auto-shop.pp.ua](https://auto-shop.pp.ua)

[google-info.pp.ua](https://google-info.pp.ua)

[buy-com.pp.ua](https://buy-com.pp.ua)

[shops-com.pp.ua](https://shops-com.pp.ua)

[rno.pp.ua](https://rno.pp.ua)

[dater.pp.ua](https://dater.pp.ua)

[ainfo.pp.ua](https://ainfo.pp.ua)

[traveled.pp.ua](https://traveled.pp.ua)

[travellead.pp.ua](https://travellead.pp.ua)

[travelling.pp.ua](https://travelling.pp.ua)

[travelled.pp.ua](https://travelled.pp.ua)


